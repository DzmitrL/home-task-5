package by.home.hometask5

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import by.home.hometask5.adapters.CustomInfoWindowForGoogleMap
import by.home.hometask5.data.DataSource
import by.home.hometask5.data.PointOnMap
import by.home.hometask5.utilities.CENTRAL_POINT_X
import by.home.hometask5.utilities.CENTRAL_POINT_Y
import by.home.hometask5.utilities.GOMEL_MAP_ZOOM
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MainActivity : AppCompatActivity(), OnMapReadyCallback {
    private lateinit var map: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val mapFragment: SupportMapFragment =
            supportFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        val centralPoint = LatLng(CENTRAL_POINT_X, CENTRAL_POINT_Y)

        map.addMarker(
            MarkerOptions()
                .icon(
                    BitmapDescriptorFactory
                        .fromResource(R.drawable.baseline_gps_not_fixed_black_24)
                )
                .position(centralPoint)
        )
        DataSource(map).loadDataAndAddMarkers()
        map.setInfoWindowAdapter(CustomInfoWindowForGoogleMap(this))
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(centralPoint, GOMEL_MAP_ZOOM))
    }


}