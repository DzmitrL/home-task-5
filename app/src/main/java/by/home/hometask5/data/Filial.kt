package by.home.hometask5.data

import com.google.gson.annotations.SerializedName

class Filial(
    @SerializedName("id")
    override val id: Int,

    @SerializedName("filial_name")
    val filialName: String,

    @SerializedName("filial_num")
    val filialNumber: String,

    @SerializedName("street_type")
    val addressType: String,

    @SerializedName("street")
    val address: String,

    @SerializedName("home_number")
    val house: String,

    @SerializedName("install_place")
    val installPlace: String,

    @SerializedName("info_worktime")
    val workTime: String,

    @SerializedName("GPS_X")
    override val gpsX: Double,

    @SerializedName("GPS_Y")
    override val gpsY: Double,

    @SerializedName("phone_info")
    val phone: String
) : PointOnMap