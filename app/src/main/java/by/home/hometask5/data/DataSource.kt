package by.home.hometask5.data

import by.home.hometask5.R
import by.home.hometask5.utilities.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.functions.Function3
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class DataSource(val map: GoogleMap) {
    fun loadDataAndAddMarkers() {
        val requestInterface = Retrofit.Builder()
            .baseUrl(BELARUSBANK_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build().create(GetData::class.java)

        Observable.zip(
            requestInterface.getGomelAtms(),
            requestInterface.getGomelInfoboxes(),
            requestInterface.getGomelFilials(),
            Function3<List<ATM>,
                    List<Infobox>,
                    List<Filial>,
                    Map<PointOnMap, Double>> { atms, infoboxes, filials ->
                return@Function3 handleLists(
                    atms,
                    infoboxes,
                    filials
                )
            }
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<Map<PointOnMap, Double>> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(t: Map<PointOnMap, Double>) {
                    val ert: List<PointOnMap> = t.keys.toTypedArray().take(
                        NUMBER_OF_NEAREST_POINTS_FOR_ARRAY
                    )


                    ert.forEach {

                        when (it) {
                            is ATM -> {
                                val mapSnippetText = """
                        ${it.addressType} ${it.address}, ${it.house}
                        
                        График работы: ${it.workTime}
                        
                        Выдаваемая валюта: ${it.currency}
                    """.trimIndent()

                                map.addMarker(
                                    MarkerOptions()
                                        .icon(
                                            BitmapDescriptorFactory
                                                .fromResource(R.drawable.baseline_atm_black_24)
                                        )
                                        .position(LatLng(it.gpsX, it.gpsY))
                                        .title(it.installPlace)
                                        .snippet(mapSnippetText)
                                )
                            }
                            is Infobox -> {
                                val mapSnippetText = """
                        ${it.addressType} ${it.address}, ${it.house}
                        
                        График работы: ${it.workTime}
                        
                        Выдаваемая валюта: ${it.currency}
                    """.trimIndent()

                                map.addMarker(
                                    MarkerOptions()
                                        .icon(
                                            BitmapDescriptorFactory
                                                .fromResource(R.drawable.baseline_local_atm_black_24)
                                        )
                                        .position(LatLng(it.gpsX, it.gpsY))
                                        .title(it.installPlace)
                                        .snippet(mapSnippetText)
                                )
                            }
                            is Filial -> {
                                val mapSnippetText = """
                        ${it.addressType} ${it.address}, ${it.house}
                        
                        График работы: ${it.workTime}
                    """.trimIndent()

                                map.addMarker(
                                    MarkerOptions()
                                        .icon(
                                            BitmapDescriptorFactory
                                                .fromResource(R.drawable.baseline_attach_money_black_24)
                                        )
                                        .position(LatLng(it.gpsX, it.gpsY))
                                        .title(it.installPlace)
                                        .snippet(mapSnippetText)
                                )
                            }
                        }
                    }
                }

                override fun onError(e: Throwable) {

                }

                override fun onComplete() {

                }

            })
    }

    private fun handleLists(
        atms: List<ATM>,
        infoboxes: List<Infobox>,
        filials: List<Filial>
    ): Map<PointOnMap, Double> {
        val map: MutableMap<PointOnMap, Double> = mutableMapOf()
        atms.forEach {
            val distance = GeometryUtils().calculateDistanceBetweenTwoPoints(
                it.gpsX,
                it.gpsY,
                CENTRAL_POINT_X,
                CENTRAL_POINT_Y
            )

            map[it] = distance
        }

        infoboxes.forEach {
            val distance = GeometryUtils().calculateDistanceBetweenTwoPoints(
                it.gpsX,
                it.gpsY,
                CENTRAL_POINT_X,
                CENTRAL_POINT_Y
            )

            map[it] = distance
        }

        filials.forEach {
            val distance = GeometryUtils().calculateDistanceBetweenTwoPoints(
                it.gpsX,
                it.gpsY,
                CENTRAL_POINT_X,
                CENTRAL_POINT_Y
            )

            map[it] = distance
        }

        val sortedMap = map.toList()
            .sortedBy { (_, value) -> value }
            .toMap()

        return sortedMap
    }
}