package by.home.hometask5.data

interface PointOnMap {
    val id: Int
    val gpsX: Double
    val gpsY: Double
}