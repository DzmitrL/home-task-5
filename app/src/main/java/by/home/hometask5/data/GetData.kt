package by.home.hometask5.data

import by.home.hometask5.utilities.GET_GOMEL_ATMS_PARAMETER
import by.home.hometask5.utilities.GET_GOMEL_FILIALS_PARAMETER
import by.home.hometask5.utilities.GET_GOMEL_INFOBOXES_PARAMETER
import io.reactivex.Observable
import retrofit2.http.GET

interface GetData {
    @GET(GET_GOMEL_ATMS_PARAMETER)
    fun getGomelAtms(): Observable<List<ATM>>

    @GET(GET_GOMEL_INFOBOXES_PARAMETER)
    fun getGomelInfoboxes(): Observable<List<Infobox>>

    @GET(GET_GOMEL_FILIALS_PARAMETER)
    fun getGomelFilials(): Observable<List<Filial>>
}