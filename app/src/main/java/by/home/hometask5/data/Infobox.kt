package by.home.hometask5.data

import com.google.gson.annotations.SerializedName

class Infobox(
    @SerializedName("id")
    override val id: Int,

    @SerializedName("address_type")
    val addressType: String,

    @SerializedName("address")
    val address: String,

    @SerializedName("house")
    val house: String,

    @SerializedName("install_place")
    val installPlace: String,

    @SerializedName("work_time")
    val workTime: String,

    @SerializedName("gps_x")
    override val gpsX: Double,

    @SerializedName("gps_y")
    override val gpsY: Double,

    @SerializedName("ATM_type")
    val atmType: String,

    @SerializedName("currency")
    val currency: String
) : PointOnMap