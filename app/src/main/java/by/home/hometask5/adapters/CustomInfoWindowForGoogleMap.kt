package by.home.hometask5.adapters

import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.TextView
import by.home.hometask5.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker

class CustomInfoWindowForGoogleMap(context: Context) : GoogleMap.InfoWindowAdapter {

    private var window = (context as Activity).layoutInflater.inflate(R.layout.custom_map_info_window, null)

    private fun renderWindowText(marker: Marker, view: View){

        val tvTitle = view.findViewById<TextView>(R.id.title)
        val tvSnippet = view.findViewById<TextView>(R.id.snippet)

        tvTitle.text = marker.title
        tvSnippet.text = marker.snippet

    }

    override fun getInfoContents(marker: Marker): View {
        renderWindowText(marker, window)
        return window
    }

    override fun getInfoWindow(marker: Marker): View? {
        renderWindowText(marker, window)
        return window
    }
}