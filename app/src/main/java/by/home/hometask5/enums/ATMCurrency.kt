package by.home.hometask5.enums

enum class ATMCurrency {
    BYN,
    USD,
    EUR
}