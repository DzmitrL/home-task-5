package by.home.hometask5.utilities

const val BELARUSBANK_URL = "https://belarusbank.by/"
const val GET_GOMEL_ATMS_PARAMETER = "api/atm?city=Гомель"
const val GET_GOMEL_INFOBOXES_PARAMETER = "api/infobox?city=Гомель"
const val GET_GOMEL_FILIALS_PARAMETER = "api/filials_info?city=Гомель"

const val CENTRAL_POINT_X = 52.425163
const val CENTRAL_POINT_Y = 31.015039

const val GOMEL_LATITUDE = 52.440513
const val GOMEL_LONGITUDE = 30.988011

const val GOMEL_MAP_ZOOM = 14.5F

const val NUMBER_OF_NEAREST_POINTS_FOR_ARRAY = 11