package by.home.hometask5.utilities

import kotlin.math.pow
import kotlin.math.sqrt

class GeometryUtils {
    fun calculateDistanceBetweenTwoPoints(x1: Double,
                                          y1: Double,
                                          x2: Double,
                                          y2: Double): Double {
        return sqrt(((x2 - x1).pow(2.0)) + ((y2 - y1).pow(2.0)))
    }
}